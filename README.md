## NAME
Chess game

## DESCRIPTION
2nd Assignment 

## REQUIREMENTS
Requires `cmake` and `cpp` <br />

## USAGE
Clone the repository using
```sh
$ git clone https://gitlab.com/tellefo92/chess-game.git
```
then navigate into the folder using
```sh
$ cd chess-game
```
Uses Cmake to build:
```sh
# Create build directory and move into it
$ mkdir -p build && cd build
# Generate makefile using Cmake - default is release
$ cmake ../
# Generate makefile for debug build
$ cmake ../ -DCMAKE_BUILD_TYPE=debug      
# Compile the project
$ make
#Run the compiled binary
$ ./main
```

## USER INSTRUCTIONS
Game of chess is strategy between two players. 

[Rules of Chess from Wikipedia](https://en.wikipedia.org/wiki/Rules_of_chess)

Please read Chess rules if you're not familiar with the game.
This game uses standard chess notations.

When you run the game you will be introduced with a screen asking 
1. New game.
2. Replay saved game
3. Continue saved game

New game
1. Two players
2. Play long as you'd like
3. Game asks user input in. (eg. a1 a2) 
4. Game ends or you decide to save the game or player decides to concede. (type 'concede' to forfeit)
5. Type 'end' to save the game. (game is saved based on date)
6. Game needs to be saved in following all lowercase, without speacial characters and spaces. (eg. mygame, mygametwo)

Replay saved game
1. Choose which saved game you'd like to watch on replay
2. Enjoy!

Continue saved game
1. Choose which saved game you'd like to continue
2. Saved game will load
3. Enjoy!

## MISSING FUNCTIONALITY
1. Castling does not work
2. En passant does not work

## DEV CONTRIBUTION
Tellef : Most of the class Game and class Board

Tino : Most of the class Piece

Most of the program was done in collaboration.

## PROGRAM FAQ
This is hot-seat Chess game.

You are able to save the game. (10 slots)

You are able to replay games.

There is few examples in saved games to test functionalities.
1. rook checking king (moving rook from e3 to e7)
2. one move away from turning pawn into queen(moving rook from b7 to b8)
3. start of the fool's mate (next move would be queen from d8 to h4)


Type : _concede_ to concede the game.

Type : _end_ to save the game.

## MAINTEINERS
Tellef Østereng | [@tellefo92](https://gitlab.com/tellefo92)

Tino Nummela | [@tinonum](https://gitlab.com/tinonum)
