#include "board.h"
#include "piece.h"

//Initializes all of the pices in to the board
Board::Board ()
{
    Piece piece;
    row.clear();
    row.push_back(Rook('W', {0, 0}));
    row.push_back(Knight('W', {0, 1}));
    row.push_back(Bishop('W', {0, 2}));
    row.push_back(Queen('W', {0, 3}));
    row.push_back(King('W', {0, 4}));
    row.push_back(Bishop('W', {0, 5}));
    row.push_back(Knight('W', {0, 6}));
    row.push_back(Rook('W', {0, 7}));
    board.push_back(row);
    row.clear();
    for (int i = 0; i < 8; ++i) {
        row.push_back(Pawn('W', {1, i}));
    }
    board.push_back(row);
    row.clear();
    for (int i = 2; i < 6; ++i) {
        for (int j = 0; j < 8; ++j) {
            row.push_back(Empty({i,j}));
        }
        board.push_back(row);
        row.clear();
    }
    for (int i = 0; i < 8; ++i) {
        row.push_back(Pawn('B', {6, i}));
    }
    board.push_back(row);
    row.clear();

    row.push_back(Rook('B', {7, 0}));
    row.push_back(Knight('B', {7, 1}));
    row.push_back(Bishop('B', {7, 2}));
    row.push_back(Queen('B', {7, 3}));
    row.push_back(King('B', {7, 4}));
    row.push_back(Bishop('B', {7, 5}));
    row.push_back(Knight('B', {7, 6}));
    row.push_back(Rook('B', {7, 7}));
    board.push_back(row);
    row.clear();
}

//
void Board::nextOpponentMoves(bool white_turn) {
    next_opponent_moves.clear();
    char color;
    Piece piece;
    if (white_turn) {
        color = 'B';
    } else {
        color = 'W';
    }
    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 8; ++j) {
            piece = board[i][j];
            if (piece.returnColor() == color) {
                //std::cout << "Getting moves for " << piece.returnColor() << " " << piece.returnType() << " at " << i << "," << j << std::endl;
                piece.getMoves(board);
                std::vector<std::pair<int,int>> possible_moves = piece.possibleMoves();
                for (size_t k = 0; k < possible_moves.size(); ++k) {
                    next_opponent_moves.push_back(possible_moves[k]);
                }
            }
        }
    }
    //std::cout << color << " can do " << next_opponent_moves.size() << " next turn" << std::endl;
}

// White king check
bool Board::whiteKingChecked(){
    for (auto pos: next_opponent_moves) {
        if (pos == white_king_pos) {
            return true;
        }
    }
    return false;
}

// Black king check
bool Board::blackKingChecked(){
    for (auto pos: next_opponent_moves) {
        if (pos == black_king_pos) {
            return true;
        }
    }
    return false;
}

// White king checkmate
bool Board::whiteKingMate(){
    Piece king = board[white_king_pos.first][white_king_pos.second];
    Piece piece;
    std::pair<int,int> pos_copy = white_king_pos;
    king.getMoves(board);
    std::vector<std::pair<int,int>> king_moves = king.possibleMoves();
    std::vector<std::pair<int,int>> next_opponent_moves_copy = next_opponent_moves;
    if (king_moves.size() == 0) {
        return true;
    } else {
        for (auto p: king_moves) {
            if (std::find(next_opponent_moves_copy.begin(), next_opponent_moves_copy.end(), p) == next_opponent_moves_copy.end()) {
                // check if moving to this position would leave the king checked again
                // copy piece at position
                piece = board[p.first][p.second];
                // move king to position
                board[p.first][p.second] = king;
                // create empty piece
                board[white_king_pos.first][white_king_pos.second] = Empty(white_king_pos);
                // update position
                king.setPosition(p);
                white_king_pos = p;
                // generate moves for opponent if king was moved to p
                next_opponent_moves.clear();
                nextOpponentMoves(true);
                if (!whiteKingChecked()) {
                    // revert changes and move king
                    board[pos_copy.first][pos_copy.second] = king;
                    king.setPosition(pos_copy);
                    white_king_pos = pos_copy;
                    next_opponent_moves = next_opponent_moves_copy;
                    board[p.first][p.second] = piece;
                    return false;
                } else {
                    board[pos_copy.first][pos_copy.second] = king;
                    king.setPosition(pos_copy);
                    white_king_pos = pos_copy;
                    next_opponent_moves = next_opponent_moves_copy;
                    board[p.first][p.second] = piece;
                }
            }
        }
    }
    return true;
}

// Black king checkmate
bool Board::blackKingMate(){
    Piece king = board[black_king_pos.first][black_king_pos.second];
    Piece piece;
    std::pair<int,int> pos_copy = black_king_pos;
    king.getMoves(board);
    std::vector<std::pair<int,int>> king_moves = king.possibleMoves();
    std::vector<std::pair<int,int>> next_opponent_moves_copy = next_opponent_moves;
    if (king_moves.size() == 0) {
        return true;
    } else {
        for (auto p: king_moves) {
            if (std::find(next_opponent_moves_copy.begin(), next_opponent_moves_copy.end(), p) == next_opponent_moves_copy.end()) {
                // check if moving to this position would leave the king checked again
                // copy piece at position
                piece = board[p.first][p.second];
                // move king to position
                board[p.first][p.second] = king;
                // create empty piece
                board[black_king_pos.first][black_king_pos.second] = Empty(black_king_pos);
                // update position
                king.setPosition(p);
                black_king_pos = p;
                // generate moves for opponent if king was moved to p
                next_opponent_moves.clear();
                nextOpponentMoves(false);
                if (!blackKingChecked()) {
                    // revert changes and move king
                    board[pos_copy.first][pos_copy.second] = king;
                    king.setPosition(pos_copy);
                    black_king_pos = pos_copy;
                    next_opponent_moves = next_opponent_moves_copy;
                    board[p.first][p.second] = piece;
                    return false;
                } else {
                    board[pos_copy.first][pos_copy.second] = king;
                    king.setPosition(pos_copy);
                    black_king_pos = pos_copy;
                    next_opponent_moves = next_opponent_moves_copy;
                    board[p.first][p.second] = piece;
                }
            }
        }
    }
    return true;
}

// Function to movie pieces
bool Board::movePiece(bool white_turn, std::pair<int,int> from_pos, std::pair<int,int> to_pos) {
    Piece piece = board[from_pos.first][from_pos.second];
    // Storing copy of piece on position we'd like to move to
    Piece piece2 = board[to_pos.first][to_pos.second];
    if (white_turn) {
        if (piece.returnColor() != 'W') {
            return false;
        }
    } else {
        if (piece.returnColor() != 'B') {
            return false;
        }
    }
    // Calculating piece moves
    piece.getMoves(board);
    // getting possible moves
    std::vector<std::pair<int,int>> possible_moves = piece.possibleMoves();
    // MAKE POSSIBLE_MOVES PUBLIC
    for (auto pos: possible_moves) {
        //std::cout << pos.first << "," << pos.second << std::endl;
        if (to_pos == pos) {
            piece.setPosition(to_pos);
            board[to_pos.first][to_pos.second] = piece;
            board[from_pos.first][from_pos.second] = Empty(from_pos);
            // Update king position if king was moved;
            if (piece.returnType() == "king") {
                if (piece.returnColor() == 'W') {
                    white_king_pos = piece.returnPosition();
                } else {
                    black_king_pos = piece.returnPosition();
                }
            }
            // Now we have to verify that the new board state does not have a checked king
            nextOpponentMoves(white_turn);
            if (white_turn) {
                if (whiteKingChecked()) {
                    piece.setPosition(from_pos);
                    board[from_pos.first][from_pos.second] = piece;
                    board[to_pos.first][to_pos.second] = piece2;
                    white_king_pos = piece.returnPosition();
                    return false;
                }
            } else {
                if (blackKingChecked()) {
                    piece.setPosition(from_pos);
                    board[from_pos.first][from_pos.second] = piece;
                    board[to_pos.first][to_pos.second] = piece2;
                    black_king_pos = piece.returnPosition();
                    return false;
                }
            }
            // changing pawns to queens when they reach end of board
            if (white_turn && piece.returnType() == "pawn" && to_pos.first == 7) {
                board[to_pos.first][to_pos.second] = Queen('W', to_pos);
            } else if (!white_turn && piece.returnType() == "pawn" && to_pos.first == 0) {
                board[to_pos.first][to_pos.second] = Queen('B', to_pos);
            }
            return true;
        }
    }
    return false;
}

std::vector<std::vector<Piece>> Board::returnBoard() {
    return board;
}

// Function to display our fancy board to the players
void Board::displayBoard(bool white_turn) {
    if (white_turn) {
        std::cout << "       a   b   c   d   e   f   g   h" << std::endl;
        std::cout << "     ┏━━━┳━━━┳━━━┳━━━┳━━━┳━━━┳━━━┳━━━┓" << std::endl;
        for (int i = 7; i >= 0; i--) {
            std::cout << "   " << i+1 << " ┃";
            for (int j = 0; j < 7; j++) {
                std::cout << " " << board[i][j].returnSymbol() << " ┃";
            }
            std::cout << " " << board[i][7].returnSymbol() << " ┃ " << i+1 << std::endl;
            if (i > 0) {
                std::cout << "     ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫" << std::endl;
            }
        }
        std::cout << "     ┗━━━┻━━━┻━━━┻━━━┻━━━┻━━━┻━━━┻━━━┛" << std::endl;
        std::cout << "       a   b   c   d   e   f   g   h" << std::endl;
    } else {
        std::cout << "       h   g   f   e   d   c   b   a" << std::endl;
        std::cout << "     ┏━━━┳━━━┳━━━┳━━━┳━━━┳━━━┳━━━┳━━━┓" << std::endl;
        for (int i = 0; i < 8; i++) {
            std::cout << "   " << i+1 << " ┃";
            for (int j = 7; j > 0; j--) {
                std::cout << " " << board[i][j].returnSymbol() << " ┃";
            }
            std::cout << " " << board[i][0].returnSymbol() << " ┃ " << i+1 << std::endl;
            if (i < 7) {
                std::cout << "     ┣━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━╋━━━┫" << std::endl;
            }
        }
        std::cout << "     ┗━━━┻━━━┻━━━┻━━━┻━━━┻━━━┻━━━┻━━━┛" << std::endl;
        std::cout << "       h   g   f   e   d   c   b   a" << std::endl;
    }
}