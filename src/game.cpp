#include "game.h"

Game::Game() {
    if (system("clear")){};
    getMode();
}

// Function to select game mode. Described below
void Game::getMode() {
    std::cout << "Select mode(input example: 1): \n";
    std::cout << "1. New game\n";
    std::cout << "2. Replay saved game\n";
    std::cout << "3. Continue saved game\n";
    std::cin >> mode;
    if (mode == 1) {
        newGame();
        return;
    } else if (mode == 2) {
        replayGame();
        return;
    } else if (mode == 3) {
        continueGame();
        return;
    } else {
        if (system("clear")){};
        std::cout << "Invalid mode!\n";
        getMode();
    }
}

// Function to create fresh brand new game
void Game::newGame() {
    std::time_t now = std::time(nullptr);
    start_time = std::ctime(&now);
    Board board = Board();
    while(true) {
        if (system("clear")){};
        board.displayBoard(white_turn);
        board.nextOpponentMoves(white_turn);
        if (white_turn) {
            if (board.whiteKingChecked()) {
                if (board.whiteKingMate()) {
                    std::cout << "Black won!" << std::endl;
                    break;
                } else {
                    std::cout << "Check!" << std::endl;
                }
            }
        } else {
            if (board.blackKingChecked()) {
                if (board.blackKingMate()) {
                    std::cout << "White won!" << std::endl;
                    break;
                } else {
                    std::cout << "Check!" << std::endl;
                }
            }
        }
        while(invalid_input) {
            takeInput();
        }
        if (end) {
            break;
        }
        if (concede) {
            if (white_turn) {
                moves += "white->concedes";
            } else {
                moves += "black->concedes";
            }
            break;
        }
        while (!board.movePiece(white_turn, from_pos, to_pos)) {
            if (system("clear")){};
            board.displayBoard(white_turn);
            std::cout << "Can't move that piece there!\n";
            takeInput();
        }
        moves += from_s + "->" + to_s + ",";
        white_turn = !white_turn;
        invalid_input = true;
    }
    game_data = std::make_pair(start_time, moves);
    save();
}

// Function to save the game. You are able to give it a name (eg. mygame) or name the save with date.
void Game::save() {
    read();
    std::string str;
    std::cout << "Give game save specific name? (Press enter to save game with date)\n";
    std::cin >> str;
    if (str.length() > 0) game_data.first = str;
    write();
}

// Function to replay saved game 
void Game::replayGame() {
    if (system("clear")){};
    read();
    int choice = 0;
    while (choice == 0 || choice > 10) {
        std::cout << "Saved games: " << std::endl;
        for (size_t i = 0; i < saved_data.size(); ++i) {
            std::string str = saved_data[i].first;
            std::cout << i + 1 << ". " << saved_data[i].first << std::endl;
        }
        std::cout << "Select save you wish to watch replay (input example: 2): ";
        std::cin >> choice;
    }
    game_data = saved_data[choice-1];
    moves = game_data.second;
    Board board = Board();
    while (true) {
        if(moves.length() == 15) {
            if (system("clear")) {};
            board.displayBoard(white_turn);
            std::cout << moves << std::endl;
            return;
        }
        int x0, y0, x1, y1;
        x0 = moves[0] - 'a';
        y0 = moves[1] - '1';
        x1 = moves[4] - 'a';
        y1 = moves[5] - '1';
        from_pos = {y0, x0};
        to_pos = {y1, x1};
        if (system("clear")) {};
        std::cout << "Replaying game" << std::endl;
        if (board.movePiece(white_turn, from_pos, to_pos)) {
            board.displayBoard(true);
            std::this_thread::sleep_for (std::chrono::seconds(2));
        }
        if (moves.length() == 7) {
            break;
        } else {
            moves = moves.substr(7, moves.length());
        }
        white_turn = !white_turn;
    }
}

// Function to continue saved game
void Game::continueGame() {
    if (system("clear")){};
    read();
    int choice = 0;
    while (choice == 0 || choice > 10) {
        std::cout << "Saved games: " << std::endl;
        for (size_t i = 0; i < saved_data.size(); ++i) {
            std::string str = saved_data[i].first;
            std::cout << i + 1 << ". " << saved_data[i].first << std::endl;
        }
        std::cout << "Select save you wish to continue (input example: 1): ";
        std::cin >> choice;
    }
    game_data = saved_data[choice-1];
    moves = game_data.second;
    moves_copy = game_data.second;
    Board board = Board();
    while (true) {
        if(moves.length() == 15) {
            if (system("clear")) {};
            board.displayBoard(white_turn);
            std::cout << moves << std::endl;
            return;
        }
        int x0, y0, x1, y1;
        x0 = moves[0] - 'a';
        y0 = moves[1] - '1';
        x1 = moves[4] - 'a';
        y1 = moves[5] - '1';
        from_pos = {y0, x0};
        to_pos = {y1, x1};
        if (board.movePiece(white_turn, from_pos, to_pos)) {};
        if (moves.length() == 7) {
            white_turn = !white_turn;
            break;
        } else {
            moves = moves.substr(7, moves.length());
        }
        white_turn = !white_turn;
    }
    moves = moves_copy;
    while(true) {
        if (system("clear")){};
        board.displayBoard(white_turn);
        board.nextOpponentMoves(white_turn);
        if (white_turn) {
            if (board.whiteKingChecked()) {
                if (board.whiteKingMate()) {
                    std::cout << "Black won!" << std::endl;
                    break;
                } else {
                    std::cout << "Check!" << std::endl;
                }
            }
        } else {
            if (board.blackKingChecked()) {
                if (board.blackKingMate()) {
                    std::cout << "White won!" << std::endl;
                    break;
                } else {
                    std::cout << "Check!" << std::endl;
                }
            }
        }
        while(invalid_input) {
            takeInput();
        }
        if (end) {
            break;
        }
        if (concede) {
            if (white_turn) {
                moves += "white->concedes";
            } else {
                moves += "black->concedes";
            }
            break;
        }
        while (!board.movePiece(white_turn, from_pos, to_pos)) {
            if (system("clear")){};
            board.displayBoard(white_turn);
            std::cout << "Can't move that piece there!\n";
            takeInput();
        }
        moves += from_s + "->" + to_s + ",";
        white_turn = !white_turn;
        invalid_input = true;
    }
    game_data = std::make_pair(start_time, moves);
    save();
}

// Function to take input from the user. (eg. a2 a4)
void Game::takeInput() {
    std::cout << std::endl;
    std::cout << "Enter move: ";
    std::cin >> from_s;
    if (from_s == "end") {
        end = true;
        invalid_input = false;
        return;
    } else if (from_s == "concede" || from_s == "retire") {
        concede = true;
        invalid_input = false;
        return;
    } else {
        std::cin >> to_s;
        if (from_s.length() != 2 || to_s.length() != 2) {
            std::cout << "Invalid command!" << std::endl;
            std::cin.clear();
            return;
        }
        int vals[4] = {from_s[1]-'1', from_s[0]-'a', to_s[1]-'1', to_s[0]-'a'};
        for (size_t i = 0; i < 4; ++i) {
            if (vals[i] < 0 || vals[i] > 7) {
                std::cout << "Invalid position!" << std::endl;
                std::cin.clear();
                return;
            }
        }
        from_pos = {vals[0], vals[1]};
        to_pos = {vals[2], vals[3]};
        invalid_input = false;
    }
}

// Function to read saved game from text file. Moves of the game are saved in pair of strings.
void Game::read() {
    std::string line;
    std::ifstream infile;
    infile.open("../game_history.txt", std::ifstream::binary);
    if (infile) {
        while(getline(infile,line)) {
            saved_data = hps::from_string<std::vector<std::pair<std::string,std::string>>>(line);
        }
        infile.close();
    } else if (mode != 1) {
        if (system("clear")){};
        std::cout << "No recorded games!" << std::endl;
        exit(0);
    }
}

// Function to write moves of the game user wants to save
void Game::write() {
    if (saved_data.size() == 10) {
        int choice = 0;
        do {
            std::cout << "You have already 10 saved games. Which game would you like to overwrite?\n";
            std::cin >> choice;
        } while (choice < 1 && choice > 10);
        saved_data[choice - 1] = game_data;
    } else {
        saved_data.push_back(game_data);
    }
    std::string serialized = hps::to_string(saved_data);
    auto parsed = hps::from_string<std::vector<std::pair<std::string,std::string>>>(serialized);
    assert(parsed == saved_data);

    std::cout << "Writing " << serialized.size() << " bytes" << std::endl;
    std::ofstream outfile;
    outfile.open("../game_history.txt", std::ofstream::binary);
    outfile << serialized << std::endl;
    outfile.close();
    std::cout << "Game saved!" << std::endl;
}