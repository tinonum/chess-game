#include "piece.h"

Piece::Piece(std::string symbol, std::string type, char color, std::pair<int,int> position) {
    this->symbol = symbol;
    this->type = type;
    this->color = color;
    this->position = position;
};

std::string Piece::returnSymbol() {
    return symbol;
}

char Piece::returnColor() {
    return color;
}

std::pair<int,int> Piece::returnPosition() {
    return position;
}

// Just a function for testing purposes, should not be actually be used otherwise
void Piece::printMoves() {
    for (auto move: possible_moves) {
        std::cout << move.first << " " << move.second << std::endl;
    }
}

void Piece::setPosition(std::pair<int,int> pos) {
    position = pos;
}

std::vector<std::pair<int,int>> Piece::possibleMoves() {
    return possible_moves;
}

std::string Piece::returnType() {
    return type;
}

void Piece::getMoves(std::vector<std::vector<Piece>> board) {
    possible_moves.clear();
    if (type == "pawn") {
        getMovesPawn(board);
    } else if (type == "rook") {
        getMovesRook(board);
    } else if (type == "knight") {
        getMovesKnight(board);
    } else if (type == "bishop") {
        getMovesBishop(board);
    } else if (type == "queen") {
        getMovesQueen(board);
    } else if (type == "king") {
        getMovesKing(board);
    } else {
        return;
    }
}

//Function how Pawn can move
void Piece::getMovesPawn(std::vector<std::vector<Piece>> board) {
    int x = position.second, y = position.first;
    if (color == 'W') {
        // Pawn in starting position
        if (y == 1) {
            if (board[y+1][x].color == ' ' && board[y+2][x].color == ' ') {
                possible_moves.push_back(board[y+2][x].position);
            }
        }
        // Straight up
        if (y < 7) {
            if (board[y+1][x].color == ' ') {
                possible_moves.push_back(board[y+1][x].position);
            }
        }
        // Up left
        if (x > 0 && y < 7) {
            if (board[y+1][x-1].color != color && board[y+1][x-1].color != ' ') {
                possible_moves.push_back(board[y+1][x-1].position);
            }
        }
        // Up right
        if (x < 7 && y < 7) {
            if (board[y+1][x+1].color != color && board[y+1][x+1].color != ' ') {
                possible_moves.push_back(board[y+1][x+1].position);
            }
        }
    } else {
        if (y == 6) {
            if (board[y-1][x].color == ' ' && board[y-2][x].color == ' ') {
                possible_moves.push_back(board[y-2][x].position);
            }
        }
        // Straight up
        if (y > 0) {
            if (board[y-1][x].color == ' ') {
                possible_moves.push_back(board[y-1][x].position);
            }
        }
        // Up left
        if (x > 0 && y > 0) {
            if (board[y-1][x-1].color != color && board[y-1][x-1].color != ' ') {
                possible_moves.push_back(board[y-1][x-1].position);
            }
        }
        // Up right
        if (x < 7 && y > 0) {
            if (board[y-1][x+1].color != color && board[y-1][x+1].color != ' ') {
                possible_moves.push_back(board[y-1][x+1].position);
            }
        }
    }
}

//Function how Rook can move
void Piece::getMovesRook(std::vector<std::vector<Piece>> board)
{   
    Piece piece;

    //Above
    for (int i = position.first + 1; i <= 7; ++i)
    {
        piece = board[i][position.second];
        if (piece.color == ' ')
        {
            possible_moves.push_back({i, position.second});
        } 
        else if (piece.color != color && piece.color != ' ')
        {
            possible_moves.push_back({i, position.second});
            break;
        } 
        else
        {
            break;
        }
    }
    //Down
    for (int i = position.first - 1; i >= 0; --i)
    {
        piece = board[i][position.second];
        if (piece.color == ' ')
        {
            possible_moves.push_back({i, position.second});
        } 
        else if (piece.color != color && piece.color != ' ')
        {
            possible_moves.push_back({i, position.second});
            break;
        }
        else 
        {
            break;
        }
    }
    // Left
    for (int i = position.second - 1; i >= 0; --i)
    {
        piece = board[position.first][i];
        if (piece.color == ' ')
        {
            possible_moves.push_back({position.first, i});
        }
        else if (piece.color != color && piece.color != ' ')
        {
            possible_moves.push_back({position.first, i});
            break;
        }
        else
        {
            break;
        }
        
    }
    // Right
    for (int i = position.second + 1; i <= 7; ++i)
    {
        piece = board[position.first][i];
        if (piece.color == ' ')
        {
            possible_moves.push_back({position.first, i});
        }
        else if (piece.color != color && piece.color != ' ')
        {
            possible_moves.push_back({position.first, i});
            break;
        }
        else
        {
            break;
        }
    }
}

//Function how Bishop can move
void Piece::getMovesBishop(std::vector<std::vector<Piece>> board)
{   
    int y = position.first;
    int x = position.second;
    Piece piece;

    // Top right
    while (x < 7 && y < 7)
    {   
        x++;
        y++;
        piece = board[y][x];
        if (piece.color == ' ')
        {
            possible_moves.push_back({y,x});
        }
        else if (piece.color != color)
        {
            possible_moves.push_back({y,x});
            break;
        }else{
            break;
        }
    }
    y = position.first;
    x = position.second;
    // Top left
    while (y > 0 && x < 7)
    {   
        y--;
        x++;
        piece = board[y][x];
        if (piece.color == ' ')
        {
            possible_moves.push_back({y,x});
        }
        else if (piece.color != color)
        {
            possible_moves.push_back({y,x});
            break;
        }
        else
        {
            break;
        }
    }
    // Bottom right
    y = position.first;
    x = position.second;
    while (y < 7 && x > 0)
    {   
        x--;
        y++;
        piece = board[y][x];
        if (piece.color == ' ')
        {
            possible_moves.push_back({y,x});
        }
        else if (piece.color != color)
        {
            possible_moves.push_back({y,x});
            break;
        }
        else
        {
            break;
        }
    }
    // Bottom left 
    y = position.first;
    x = position.second;
    while (y > 0 && x > 0)
    {   
        x--;
        y--;
        piece = board[y][x];
        if (piece.color == ' ')
        {
            possible_moves.push_back({y,x});
        }
        else if (piece.color != color)
        {
            possible_moves.push_back({y,x});
            break;
        }
        else
        {
            break;
        }
    }
}

//Function how Queen can move
void Piece::getMovesQueen(std::vector<std::vector<Piece>> board)
{   
    int y = position.first;
    int x = position.second;
    Piece piece;

    // Top right
    while (x < 7 && y < 7)
    {   
        x++;
        y++;
        piece = board[y][x];
        if (piece.color == ' ')
        {
            possible_moves.push_back({y,x});
        }
        else if (piece.color != color)
        {
            possible_moves.push_back({y,x});
            break;
        }else{
            break;
        }
    }
    y = position.first;
    x = position.second;
    // Top left
    while (x > 0 && y < 7)
    {   
        x--;
        y++;
        piece = board[y][x];
        if (piece.color == ' ')
        {
            possible_moves.push_back({y,x});
        }
        else if (piece.color != color)
        {
            possible_moves.push_back({y,x});
            break;
        }
        else
        {
            break;
        }
    }
    // Bottom right
    y = position.first;
    x = position.second;
    while (y > 0 && x < 7)
    {   
        x++;
        y--;
        piece = board[y][x];
        if (piece.color == ' ')
        {
            possible_moves.push_back({y,x});
        }
        else if (piece.color != color)
        {
            possible_moves.push_back({y,x});
            break;
        }
        else
        {
            break;
        }
    }
    // Bottom left 
    y = position.first;
    x = position.second;
    while (y > 0 && x > 0)
    {   
        x--;
        y--;
        piece = board[y][x];
        if (piece.color == ' ')
        {
            possible_moves.push_back({y,x});
        }
        else if (piece.color != color)
        {
            possible_moves.push_back({y,x});
            break;
        }
        else
        {
            break;
        }
    }
    //Above
    for (int i = position.first + 1; i <= 7; ++i)
    {
        piece = board[i][position.second];
        if (piece.color == ' ')
        {
            possible_moves.push_back({i, position.second});
        } 
        else if (piece.color != color && piece.color != ' ')
        {
            possible_moves.push_back({i, position.second});
            break;
        } 
        else
        {
            break;
        }
    }
    //Down
    for (int i = position.first - 1; i >= 0; --i)
    {
        piece = board[i][position.second];
        if (piece.color == ' ')
        {
            possible_moves.push_back({i, position.second});
        } 
        else if (piece.color != color && piece.color != ' ')
        {
            possible_moves.push_back({i, position.second});
            break;
        }
        else 
        {
            break;
        }
    }
    // Left
    for (int i = position.second - 1; i >= 0; --i)
    {
        piece = board[position.first][i];
        if (piece.color == ' ')
        {
            possible_moves.push_back({position.first, i});
        }
        else if (piece.color != color && piece.color != ' ')
        {
            possible_moves.push_back({position.first, i});
            break;
        }
        else
        {
            break;
        }
        
    }
    // Right
    for (int i = position.second + 1; i <= 7; ++i)
    {
        piece = board[position.first][i];
        if (piece.color == ' ')
        {
            possible_moves.push_back({position.first, i});
        }
        else if (piece.color != color && piece.color != ' ')
        {
            possible_moves.push_back({position.first, i});
            break;
        }
        else
        {
            break;
        }
    }
}

//Function how Knight can move
void Piece::getMovesKnight(std::vector<std::vector<Piece>> board)
{   
    int y = position.first;
    int x = position.second;
    Piece piece;

    //Up left
    if (y < 6 && x > 0) {
        piece = board[y+2][x-1];
        if (piece.color != color)
        {
            possible_moves.push_back({position.first+2, position.second-1});
        }
    }
    //Up right
    if (y < 6 && x < 7) {
        piece = board[y+2][x+1];
        if (piece.color != color)
        {
            possible_moves.push_back({position.first+2, position.second+1});
        }
    }
    //Down left
    if (y > 1 && x > 0) {
        piece = board[y-2][x-1];
        if (piece.color != color)
        {
            possible_moves.push_back({position.first-2, position.second-1});
        }
    }
    //Down right
    if (y > 1 && x < 7) {
        piece = board[y-2][x+1];
        if (piece.color != color)
        {
            possible_moves.push_back({position.first-2, position.second+1});
        }
    }
    //Left down 
    if (y > 0 && x > 1) {
        piece = board[y-1][x-2];
        if (piece.color != color)
        {
            possible_moves.push_back({position.first-1, position.second-2});
        }
    }
    //Left up 
    if (y < 7 && x > 1) {
        piece = board[y+1][x-2];
        if (piece.color != color)
        {
            possible_moves.push_back({position.first+1, position.second-2});
        }
    }
    //Right down
    if (y > 0 && x < 6) {
        piece = board[y-1][x+2];
        if (piece.color != color)
        {
            possible_moves.push_back({position.first-1, position.second+2});
        }
    }
    //Right up
    if (y < 7 && x < 6) {
        piece = board[y+1][x+2];
        if (piece.color != color)
        {
            possible_moves.push_back({position.first+1, position.second+2});
        }
    }
}

// Function how King can move
void Piece::getMovesKing(std::vector<std::vector<Piece>> board) {
    int x = position.second;
    int y = position.first;
    for (int i = -1; i < 2; ++i) {
        for (int j = -1; j < 2; ++j) {
            if (x+j >= 0 && x+j < 8 && y+i >= 0 && y+i < 8) {
                if (board[y+i][x+j].color != color) {
                    possible_moves.push_back({y+i, x+j});
                }
            }
        }
    }
}

Empty::Empty(std::pair<int,int> position) : Piece::Piece(" ", " ", ' ', position) {};

//Functions to initialize symbols for each piece
Pawn::Pawn(char color, std::pair<int,int> position) : Piece::Piece("0", "pawn", color, position) {
    if (color == 'B') {
        symbol = "♙";
    } else {
        symbol = "♟";
    }
}

Rook::Rook(char color, std::pair<int,int> position) : Piece::Piece("0", "rook", color, position) {
    if (color == 'B') {
        symbol = "♖";
    } else {
        symbol = "♜";
    }
}

Knight::Knight(char color, std::pair<int,int> position) : Piece::Piece("0", "knight", color, position) {
    if (color == 'B') {
        symbol = "♘";
    } else {
        symbol = "♞";
    }
}

Bishop::Bishop(char color, std::pair<int,int> position) : Piece::Piece("0", "bishop", color, position) {
    if (color == 'B') {
        symbol = "♗";
    } else {
        symbol = "♝";
    }
}

Queen::Queen(char color, std::pair<int,int> position) : Piece::Piece("0", "queen", color, position) {
    if (color == 'B') {
        symbol = "♕";
    } else {
        symbol = "♛";
    }
}

King::King(char color, std::pair<int,int> position) : Piece::Piece("0", "king", color, position) {
    if (color == 'B') {
        symbol = "♔";
    } else {
        symbol = "♚";
    }
}